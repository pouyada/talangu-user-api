
![Alt Laravel](https://laravel.com/laravel.png)


## About The project
```bash
Laravel is accessible, yet powerful, providing tools needed for large, robust applications.
A superb combination of simplicity, elegance, and innovation give you tools you need to build any application with which you are tasked.
```

## Learning Laravel
```bash
All the documentions are avable here https://laravel.com/docs/5.3
```

## Installing Notes
```bash
1 - we use laravel framework 5.3.* for this project.
```
```bash
2 - To use json error response for APIs added code to App/Exeptios/Handler render() function.
```
```bash
3 - Using Fractal : To use "league/fractal" ( http://fractal.thephpleague.com ), we use "spatie/laravel-fractal 2.0" Fractal service provider ( https://github.com/spatie/laravel-fractal ).
 "sorskod/larasponse" does not work with laravel 5.3 yet ( https://github.com/salebab/larasponse )
```
 
 
 
