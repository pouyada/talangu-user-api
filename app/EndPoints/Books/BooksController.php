<?php

namespace App\EndPoints\Books;

use Illuminate\Http\Request;
use App\Transformers\BookTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\Book;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\JsonApiSerializer;

/**
 * Class BooksController
 * @package Modules\Books\Controllers
 */
class BooksController extends Controller
{
    /**
     *
     * GET /books
     * @return array
     */
    public function fetchAll()
    {
      /* return fractal()
            ->collection(Book::paginate(20))
            ->transformWith(new BookTransformer())
            ->toArray();
      */

        $books = Book::paginate(env('PAGE_SIZE'));

        return fractal()
            ->collection($books, new BookTransformer())
            ->paginateWith(new IlluminatePaginatorAdapter($books));
    }

    /**
     * GET /books/{id}
     * @param integer $id
     * @return mixed
     */
    public function fetch($id)
    {
        return fractal()
            ->item(Book::findOrFail($id))
            ->transformWith(new BookTransformer());
    }

    /**
     * POST /books
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'author_id' => 'required|exists:authors,id'
        ], [
            'description.required' => 'Please provide a :attribute.'
        ]);

        $book = Book::create($request->all());
        $data = $this->item($book, new BookTransformer());

        return response()->json($data, 201, [
            'Location' => route('books.show', ['id' => $book->id])
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $book = Book::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => 'Book not found'
                ]
            ], 404);
        }

        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required',
            'author_id' => 'exists:authors,id'
        ], [
            'description.required' => 'Please provide a :attribute.'
        ]);

        $book->fill($request->all());
        $book->save();

        return $this->item($book, new BookTransformer());
    }

    /**
     * @param $id
     * @return \Laravel\Lumen\Http\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function delete($id)
    {
        try {
            $book = Book::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'error' => [
                    'message' => 'Book not found'
                ]
            ], 404);
        }

        $book->delete();

        return response(null, 204);
    }

/*
    public function fetch($id){

        $headersToPass = ['Content-Type', 'application/json'];

        $body = array(

        );

        return response( 'body', 404,
            array_only(
                $headersToPass,
                $headersToPass
            )

        );
        // return array('key' => 'Test Pouya '.$id );
    }
*/
}
