<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


Route::group(['namespace' => 'App\EndPoints\Books'], function ($group) {

    $group->get('/books', 'BooksController@fetchAll');

   /* $group->get('books', function () {
        return App\Models\Book::paginate(20);
    });*/

    $group->get('/books/{id}', 'BooksController@fetch');

    $group->post('/books', 'BooksController@create');
    $group->put('/books/{id}', 'BooksController@update');
    $group->delete('/books/{id}', 'BooksController@delete');

});

