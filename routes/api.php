<?php

use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return array('fff' => 'ddd');
        //$request->user();
});
    //->middleware('auth:api');

$file_system = new Filesystem();
$modules = $file_system->directories(app_path('EndPoints'));//__DIR__ . '/../Modules'

foreach ($modules as $module) {
    if (file_exists($module . '/route.php')) {
        require $module . '/route.php';
    }
}

$router->get('{path?}', function($path)
{
    echo 'Route mismatch ' . $path;
})->where('path', '.*');


