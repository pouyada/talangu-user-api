<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

Route::get('/home', 'App\Http\Controllers\HomeController@index');

// Authentication Routes...
Route::get('login', 'App\Endpoints\Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'App\Endpoints\Auth\LoginController@login');
Route::post('logout', 'App\Endpoints\Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'App\Endpoints\Auth\RegisterController@showRegistrationForm');
Route::post('register', 'App\Endpoints\Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'App\Endpoints\Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'App\Endpoints\Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'App\Endpoints\Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'App\Endpoints\Auth\ResetPasswordController@reset');


Route::any('oauth/clients', function () {
    return 'welcome';
});